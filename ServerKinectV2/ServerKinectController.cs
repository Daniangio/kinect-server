﻿using Microsoft.Kinect;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Numerics;
using Newtonsoft.Json;

namespace ServerKinectV2
{

    //TODO Record window

    class ServerKinectController
    {
        public static ServerKinectController Instance;

        private static Body[] bodies;

        private static SerializableSkeleton[] skeletons;
        public List<GestureDetector> gestureDetectorList = null;

        private static MainWindow relay;

        private static int _timespan = 200;
        private static int _window = 1;
        private static string _option = "Polling";
        private static bool isactive;

        public int getTimeSpan() {
            return (int)(1000/_timespan);
        }
        public int getWindow()
        {
            return _window;
        }

        private Thread samplingthread;

        public ServerKinectController(MainWindow r) {
            Instance = this;
            relay = r;
            skeletons = new SerializableSkeleton[6];
            gestureDetectorList = new List<GestureDetector>();
            for (int i = 0; i < 6; i++) {
                SerializableSkeleton s = new SerializableSkeleton();
                GestureDetector detector = new GestureDetector(relay, s);
                skeletons[i] = s;
                gestureDetectorList.Add(detector);
            }
            isactive = false;
            new ServerSender();
            new ServerListener();
        }

        public void startSamplig() {
            samplingthread = new Thread(ServerKinectController.sampleReader);
            isactive = true;
            samplingthread.Start();
            relay.UpdateConsole("Start sampling");
        }

        public void stopSamplig() {
            isactive = false;
            relay.UpdateConsole("Stop sampling \n");
        }

        private static void sampleReader() {
            while (isactive) {
                Thread.Sleep(_timespan);
                readBody();
                for(int i=0; i<skeletons.Length; i++)
                {
                    relay.UpdateConsole("body #" + i.ToString() + skeletons[i].SpineBase.ToString());
                }
                relay.UpdateConsole("Read new position");
                if (_option == "Streaming")
                {
                    ServerSender.Instance.sendStatus((_window == 1) ? "Single" : "Window");
                }
            }
        }



        private static void readBody() {
            bodies = relay.bodies;
            int i = 0;

            foreach (Body b in bodies) {
                
                if (b != null) {
                    if (b.IsTracked)
                    {
                        foreach (JointType t in Enum.GetValues(typeof(JointType)))
                        {
                            Joint j = b.Joints[t];
                            switch (t)
                            {
                                case JointType.SpineBase:
                                    skeletons[i].SpineBase.X = j.Position.X;
                                    skeletons[i].SpineBase.Y = j.Position.Y;
                                    skeletons[i].SpineBase.Z = j.Position.Z;
                                    break;
                                case JointType.SpineMid:
                                    skeletons[i].SpineMid.X = j.Position.X;
                                    skeletons[i].SpineMid.Y = j.Position.Y;
                                    skeletons[i].SpineMid.Z = j.Position.Z;
                                    break;
                                case JointType.Neck:
                                    skeletons[i].Neck.X = j.Position.X;
                                    skeletons[i].Neck.Y = j.Position.Y;
                                    skeletons[i].Neck.Z = j.Position.Z;
                                    break;
                                case JointType.Head:
                                    skeletons[i].Head.X = j.Position.X;
                                    skeletons[i].Head.Y = j.Position.Y;
                                    skeletons[i].Head.Z = j.Position.Z;
                                    break;
                                case JointType.ShoulderLeft:
                                    skeletons[i].ShoulderLeft.X = j.Position.X;
                                    skeletons[i].ShoulderLeft.Y = j.Position.Y;
                                    skeletons[i].ShoulderLeft.Z = j.Position.Z;
                                    break;
                                case JointType.ElbowLeft:
                                    skeletons[i].ElbowLeft.X = j.Position.X;
                                    skeletons[i].ElbowLeft.Y = j.Position.Y;
                                    skeletons[i].ElbowLeft.Z = j.Position.Z;
                                    break;
                                case JointType.WristLeft:
                                    skeletons[i].WristLeft.X = j.Position.X;
                                    skeletons[i].WristLeft.Y = j.Position.Y;
                                    skeletons[i].WristLeft.Z = j.Position.Z;
                                    break;
                                case JointType.HandLeft:
                                    skeletons[i].HandLeft.X = j.Position.X;
                                    skeletons[i].HandLeft.Y = j.Position.Y;
                                    skeletons[i].HandLeft.Z = j.Position.Z;
                                    break;
                                case JointType.ShoulderRight:
                                    skeletons[i].ShoulderRight.X = j.Position.X;
                                    skeletons[i].ShoulderRight.Y = j.Position.Y;
                                    skeletons[i].ShoulderRight.Z = j.Position.Z;
                                    break;
                                case JointType.ElbowRight:
                                    skeletons[i].ElbowRight.X = j.Position.X;
                                    skeletons[i].ElbowRight.Y = j.Position.Y;
                                    skeletons[i].ElbowRight.Z = j.Position.Z;
                                    break;
                                case JointType.WristRight:
                                    skeletons[i].WristRight.X = j.Position.X;
                                    skeletons[i].WristRight.Y = j.Position.Y;
                                    skeletons[i].WristRight.Z = j.Position.Z;
                                    break;
                                case JointType.HandRight:
                                    skeletons[i].HandRight.X = j.Position.X;
                                    skeletons[i].HandRight.Y = j.Position.Y;
                                    skeletons[i].HandRight.Z = j.Position.Z;
                                    break;
                                case JointType.HipLeft:
                                    skeletons[i].HipLeft.X = j.Position.X;
                                    skeletons[i].HipLeft.Y = j.Position.Y;
                                    skeletons[i].HipLeft.Z = j.Position.Z;
                                    break;
                                case JointType.KneeLeft:
                                    skeletons[i].KneeLeft.X = j.Position.X;
                                    skeletons[i].KneeLeft.Y = j.Position.Y;
                                    skeletons[i].KneeLeft.Z = j.Position.Z;
                                    break;
                                case JointType.AnkleLeft:
                                    skeletons[i].AnkleLeft.X = j.Position.X;
                                    skeletons[i].AnkleLeft.Y = j.Position.Y;
                                    skeletons[i].AnkleLeft.Z = j.Position.Z;
                                    break;
                                case JointType.FootLeft:
                                    skeletons[i].FootLeft.X = j.Position.X;
                                    skeletons[i].FootLeft.Y = j.Position.Y;
                                    skeletons[i].FootLeft.Z = j.Position.Z;
                                    break;
                                case JointType.HipRight:
                                    skeletons[i].HipRight.X = j.Position.X;
                                    skeletons[i].HipRight.Y = j.Position.Y;
                                    skeletons[i].HipRight.Z = j.Position.Z;
                                    break;
                                case JointType.KneeRight:
                                    skeletons[i].KneeRight.X = j.Position.X;
                                    skeletons[i].KneeRight.Y = j.Position.Y;
                                    skeletons[i].KneeRight.Z = j.Position.Z;
                                    break;
                                case JointType.AnkleRight:
                                    skeletons[i].ElbowLeft.X = j.Position.X;
                                    skeletons[i].ElbowLeft.Y = j.Position.Y;
                                    skeletons[i].ElbowLeft.Z = j.Position.Z;
                                    break;
                                case JointType.FootRight:
                                    skeletons[i].FootRight.X = j.Position.X;
                                    skeletons[i].FootRight.Y = j.Position.Y;
                                    skeletons[i].FootRight.Z = j.Position.Z;
                                    break;
                                case JointType.SpineShoulder:
                                    skeletons[i].SpineShoulder.X = j.Position.X;
                                    skeletons[i].SpineShoulder.Y = j.Position.Y;
                                    skeletons[i].SpineShoulder.Z = j.Position.Z;
                                    break;
                                case JointType.HandTipLeft:
                                    skeletons[i].HandTipLeft.X = j.Position.X;
                                    skeletons[i].HandTipLeft.Y = j.Position.Y;
                                    skeletons[i].HandTipLeft.Z = j.Position.Z;
                                    break;
                                case JointType.ThumbLeft:
                                    skeletons[i].ThumbLeft.X = j.Position.X;
                                    skeletons[i].ThumbLeft.Y = j.Position.Y;
                                    skeletons[i].ThumbLeft.Z = j.Position.Z;
                                    break;
                                case JointType.HandTipRight:
                                    skeletons[i].HandTipRight.X = j.Position.X;
                                    skeletons[i].HandTipRight.Y = j.Position.Y;
                                    skeletons[i].HandTipRight.Z = j.Position.Z;
                                    break;
                                case JointType.ThumbRight:
                                    skeletons[i].ThumbRight.X = j.Position.X;
                                    skeletons[i].ThumbRight.Y = j.Position.Y;
                                    skeletons[i].ThumbRight.Z = j.Position.Z;
                                    break;
                            }
                            //NB: positions is a vector3 in meters Y Up, Z Exit
                        }
                    }
                    else
                    {
                        relay.UpdateConsole("##########BODY RESETTATO");
                        skeletons[i].resetPosition();
                    }
                        
                }
                i++;

            }

            /*
             * skeletons = skeletons;
            skeletons.Add(skeletons);
            if (skeletons.Count > _window) {
                skeletons.RemoveAt(0);
            }
            */
        }



        public void setSamplingTime(int i) {
            _timespan = i;
            relay.UpdateConsole("Set timespan to " + _timespan);
            relay.updateParameters();
        }
        public void setWindow(int i)
        {
            _window = i;
            relay.UpdateConsole("Set window to " + _window);
            relay.updateParameters();
        }
        public void setOption(string o)
        {
            _option = o;
            relay.UpdateConsole("Set option to " + _option);
        }

        public string serializeDataToTransferSingle() {
            string json = JsonConvert.SerializeObject(skeletons);
            return json;
        }


        //da sistemare
        public string serializeDataToTransferWindow()
        {

            string json = JsonConvert.SerializeObject(skeletons);
            /*
            string json = "[";
            foreach (SerializableSkeleton[] s in skeletons)
            {
                json += "[";
                foreach (SerializableSkeleton sitem in s)
                {
                    json += sitem.GetEncode();
                }
                json += "],";
            }
            json = json.Remove(json.Length - 1);
            json += "]";
            */
            return json;
        }

   
    }

}