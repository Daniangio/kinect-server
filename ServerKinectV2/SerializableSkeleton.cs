﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Numerics;

namespace ServerKinectV2
{
    [DataContract]
    class SerializableSkeleton
    {
        [DataMember]
        public Vector3 SpineBase;
        [DataMember]
        public Vector3 SpineMid;
        [DataMember]
        public Vector3 Neck;
        [DataMember]
        public Vector3 Head;
        [DataMember]
        public Vector3 ShoulderLeft;
        [DataMember]
        public Vector3 ElbowLeft;
        [DataMember]
        public Vector3 WristLeft;
        [DataMember]
        public Vector3 HandLeft;
        [DataMember]
        public Vector3 ShoulderRight;
        [DataMember]
        public Vector3 ElbowRight;
        [DataMember]
        public Vector3 WristRight;
        [DataMember]
        public Vector3 HandRight;
        [DataMember]
        public Vector3 HipLeft;
        [DataMember]
        public Vector3 KneeLeft;
        [DataMember]
        public Vector3 AnkleLeft;
        [DataMember]
        public Vector3 FootLeft;
        [DataMember]
        public Vector3 HipRight;
        [DataMember]
        public Vector3 KneeRight;
        [DataMember]
        public Vector3 AnkleRight;
        [DataMember]
        public Vector3 FootRight;
        [DataMember]
        public Vector3 SpineShoulder;
        [DataMember]
        public Vector3 HandTipLeft;
        [DataMember]
        public Vector3 ThumbLeft;
        [DataMember]
        public Vector3 HandTipRight;
        [DataMember]
        public Vector3 ThumbRight;


        [DataMember]
        public string GestureMask = "0";

        public SerializableSkeleton()
        {
            SpineBase = new Vector3(0, 0, 0);
            SpineMid = new Vector3(0, 0, 0);
            Neck = new Vector3(0, 0, 0);
            Head = new Vector3(0, 0, 0);
            ShoulderLeft = new Vector3(0, 0, 0);
            ElbowLeft = new Vector3(0, 0, 0);
            WristLeft = new Vector3(0, 0, 0);
            HandLeft = new Vector3(0, 0, 0);
            ShoulderRight = new Vector3(0, 0, 0);
            ElbowRight = new Vector3(0, 0, 0);
            WristRight = new Vector3(0, 0, 0);
            HandRight = new Vector3(0, 0, 0);
            HipLeft = new Vector3(0, 0, 0);
            HipRight = new Vector3(0, 0, 0);
            KneeLeft = new Vector3(0, 0, 0);
            KneeRight = new Vector3(0, 0, 0);
            AnkleLeft = new Vector3(0, 0, 0);
            AnkleRight = new Vector3(0, 0, 0);
            FootLeft = new Vector3(0, 0, 0);
            FootRight = new Vector3(0, 0, 0);
            SpineShoulder = new Vector3(0, 0, 0);
            HandTipLeft = new Vector3(0, 0, 0);
            HandTipRight = new Vector3(0, 0, 0);
            ThumbLeft = new Vector3(0, 0, 0);
            ThumbRight = new Vector3(0, 0, 0);
        }

        public void setBending(bool status)
        {
            char[] temp = GestureMask.ToCharArray();
            temp[0] = status ? '1': '0' ;
            GestureMask = new string(temp);
        }

        public void resetPosition()
        {

            SpineBase.X = 0;
            SpineBase.Y = 0;
            SpineBase.Z = 0;

            SpineMid.X = 0;
            SpineMid.Y = 0;
            SpineMid.Z = 0;

            Neck.X = 0;
            Neck.Y = 0;
            Neck.Z = 0;

            Head.X = 0;
            Head.Y = 0;
            Head.Z = 0;

            ShoulderLeft.X = 0;
            ShoulderLeft.Y = 0;
            ShoulderLeft.Z = 0;

            ElbowLeft.X = 0;
            ElbowLeft.Y = 0;
            ElbowLeft.Z = 0;

            WristLeft.X = 0;
            WristLeft.Y = 0;
            WristLeft.Z = 0;

            HandLeft.X = 0;
            HandLeft.Y = 0;
            HandLeft.Z = 0;

            ShoulderRight.X = 0;
            ShoulderRight.Y = 0;
            ShoulderRight.Z = 0;

            ElbowRight.X = 0;
            ElbowRight.Y = 0;
            ElbowRight.Z = 0;

            WristRight.X = 0;
            WristRight.Y = 0;
            WristRight.Z = 0;

            HandRight.X = 0;
            HandRight.Y = 0;
            HandRight.Z = 0;

            HipLeft.X = 0;
            HipLeft.Y = 0;
            HipLeft.Z = 0;

            KneeLeft.X = 0;
            KneeLeft.Y = 0;
            KneeLeft.Z = 0;

            AnkleLeft.X = 0;
            AnkleLeft.Y = 0;
            AnkleLeft.Z = 0;

            FootLeft.X = 0;
            FootLeft.Y = 0;
            FootLeft.Z = 0;

            HipRight.X = 0;
            HipRight.Y = 0;
            HipRight.Z = 0;

            KneeRight.X = 0;
            KneeRight.Y = 0;
            KneeRight.Z = 0;

            AnkleRight.X = 0;
            AnkleRight.Y = 0;
            AnkleRight.Z = 0;

            FootRight.X = 0;
            FootRight.Y = 0;
            FootRight.Z = 0;

            SpineShoulder.X = 0;
            SpineShoulder.Y = 0;
            SpineShoulder.Z = 0;

            HandTipLeft.X = 0;
            HandTipLeft.Y = 0;
            HandTipLeft.Z = 0;

            ThumbLeft.X = 0;
            ThumbLeft.Y = 0;
            ThumbLeft.Z = 0;

            HandTipRight.X = 0;
            HandTipRight.Y = 0;
            HandTipRight.Z = 0;

            ThumbRight.X = 0;
            ThumbRight.Y = 0;
            ThumbRight.Z = 0;

            GestureMask = "0";
        }

        private Vector3 resetVector(Vector3 temp)
        {
            temp.X = 0;
            temp.Y = 0;
            temp.Z = 0;
            return temp;
        }
        
    }
}