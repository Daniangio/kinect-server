﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using WebServer;

namespace ServerKinectV2
{
    /// <summary>
    /// This class inherit from ServerController, and in this contest is used as ModelView
    /// Infact it is set as DataContext.
    /// ServerController is a library in the Gloss project
    /// This ServerListerner shouldn't know nothing about GUI!
    /// ServerController base class integrates the INotityPropertyChange, but in this case I don't need it!
    /// </summary>
    public class ServerListener : ServerController
    {
        public class EventArgsSingleMessage : EventArgs
        {
            public EventArgsSingleMessage(string message)
            {
                Message = message;
            }
            public string Message { get; set; }
        }
        public event EventHandler<EventArgsSingleMessage> MessageReceivedEvent;

        private ServerKinectController controller { get; }

        private static ServerListener _sl;
        public static ServerListener Instance
        {
            get
            {
                if (_sl == null)
                {
                    _sl = new ServerListener();
                }
                return _sl;
            }
        }
        public  ServerListener()
        {
            controller = ServerKinectController.Instance;
            StartServer();
        }

        public void StartServer()
        {
            PortEnabled = 7080;
            ServerAddresses.Add("http://*:" + PortEnabled + "/");
            StartServerHTTP();
            Console.WriteLine("Server listening on port: " + PortEnabled);
        }

        internal void CloseServer()
        {
            DisablePort();
        }

        /// <summary>
        /// Event handler call when the webserver receive a request.
        /// The registration to MessageReviedEvent is done in the base class
        /// </summary>
        /// <param name="e"></param>
        public override void MessageReceived(WebServer.WebServer.RequestReceivedEventArgs e)
        {
            //ConsoleText = e.Message;
            MessageReceivedEvent?.Invoke(this, new EventArgsSingleMessage(e.Message));
            Console.WriteLine(e.Message);
            try
            {
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(Message));
                MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(e.Message));
                Message mes = (Message)ser.ReadObject(stream);
                Console.WriteLine(mes.type);

                if (mes.type == "KinectCommand")
                {
                    switch (mes.command) {
                        case "SetUpKinect":
                            controller.setSamplingTime((int)(1000 / mes.frequency));
                            controller.setWindow(mes.window);
                            ServerSender.Instance.setListeningAddress(mes.listeningaddress);
                            break;
                        case "StartKinect":
                            controller.setOption(mes.option);
                            controller.startSamplig();
                            break;
                        case "StopKinect":
                            controller.stopSamplig();
                            break;
                        case "ReadKinect":
                            ServerSender.Instance.sendStatus(mes.option);
                            break;
                    }
                    
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
    [DataContract]
    public class Message
    {
        [DataMember]
        public string type { get; set; }
        [DataMember]
        public string command { get; set; }
        [DataMember]
        public int frequency { get; set; }
        [DataMember]
        public int window { get; set; }
        [DataMember]
        public string listeningaddress { get; set; }
        [DataMember]
        public string option { get; set; }
    }
}
