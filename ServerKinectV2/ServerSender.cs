﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ServerKinectV2
{
    public class ServerSender
    {
        public static ServerSender Instance;

        private string _listeningAddress = "";

        private static readonly HttpClient client = new HttpClient();

        public ServerSender(){
            Instance = this;
        }

        public void setListeningAddress(string add)
        {
            _listeningAddress = add;
        }

        public async void sendStatus(string option) {

            if (_listeningAddress == "" || _listeningAddress == null) {
                return;
            }

            //WE WANT JUST THE DATA, NO HEADER
            /*
            string serialized = "{\"type\":\"KinectRead\", \"frames\" : \"";
            if (option == "Single") {
                serialized += ServerKinectController.Instance.serializeDataToTransferSingle() + "\"}";
            }
            if (option == "Window")
            {
                serialized += ServerKinectController.Instance.serializeDataToTransferWindow() + "\"}";
            }
            */
            try
            {
                string response = await sendPostCommand(_listeningAddress, ServerKinectController.Instance.serializeDataToTransferSingle());
            }
            catch
            {
                Console.WriteLine("######  ECCEZIONE  ########");
            }
        }

        public static async Task<string> sendPostCommand(string ip, string json)
        {
            Console.WriteLine(json);
            var response = await client.PostAsync(ip, new StringContent(json, Encoding.UTF8, "application/json"));
            Console.WriteLine("######  BBBBBBBB  ########");
            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine("###### CCCCCCC  ########");
            return responseString;

        }
    }
}