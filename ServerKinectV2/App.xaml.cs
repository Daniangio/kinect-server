﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace ServerKinectV2
{
    /// <summary>
    /// Logica di interazione per App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            MessageBox.Show("Mi dispiace, si è verificato un errore inatteso: " + e.Exception.Message, "Errore", MessageBoxButton.OK, MessageBoxImage.Error);
            e.Handled = true;
        }
    }
}
